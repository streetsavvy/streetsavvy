var api_home = 'request/fire?' 

function getURL(){
    var end = document.getElementById("end").value;
    var start = document.getElementById("start").value;
    var re = new RegExp("^([0-9]{2}:(0[1-9]|11|12):[0-9]{4})$");
    console.log( end );
    console.log( start );
    if (!validateField(start, 'Start Date', re) || !validateField(end, 'End Date', re)){ 
        return null;
    }
    var api_url = api_home + 'start=' + start + '&stop=' + end;
    console.log(api_url);
    return api_url;
}

function addData(table, data){
    var newRows = [];
    var len = data.length;
    for (var i = 0; i < len; i++) {
        var r = [
            data[i].address,
            data[i].call_final_disposition,
            data[i].call_type,
            data[i].received_dttm,
            data[i].neighborhood_district,
            data[i].unit_type,
            data[i].number_of_alarms
        ]
        newRows.push(r);
    };
    console.log(newRows);
    table.rows.add(newRows);
}


function updateTable(table, url){
    table.clear().draw(false);
    var spinner = new Spinner(opts).spin(spin_target);
    $.getJSON( url, function( data ) {
        console.log(data);
        if (typeof data === 'string' || data instanceof String){
            generate_alert(data);
        } else{ 
            addData(table, data);
        }
        table.draw();
        spinner.stop();
    });
}

$(document).ready(function() {
    var t = $('#fire_table').DataTable({searching: false, lengthChange: false});
    
    $( '#submit' ).click(function() {
        var u = getURL();
        if (!u){
            t.clear().draw();
        } else {
            updateTable(t, u);
        }
    } );
} );



