$(document).foundation()
    
function generate_alert(message) {
    var api_alert = '<div class="alert callout" data-closable>' +
    '<h5>'+ message + '</h5>' +
    '<button class="close-button" aria-label="Dismiss alert" type="button" data-close>' +
    '<span aria-hidden="true">&times;</span></button></div>';
    $( ".js-alerts" ).after(api_alert);}

function validateField(field, name, re){
    if (!field){
        generate_alert('Missing '+name);
        return false;
    }
    if (!re.test(field)){
        generate_alert(name+' Improperly Formatted')
        return false;
    }
    return true;
}

var opts = {
    lines: 13, // The number of lines to draw
    length: 7, // The length of each line
    width: 4, // The line thickness
    radius: 10, // The radius of the inner circle
    corners: 1, // Corner roundness (0..1)
    rotate: 0, // The rotation offset
    color: '#000', // #rgb or #rrggbb
    speed: 1, // Rounds per second
    trail: 60, // Afterglow percentage
    shadow: false, // Whether to render a shadow
    hwaccel: false, // Whether to use hardware acceleration
    className: 'spinner', // The CSS class to assign to the spinner
    scale: 4
};
var spin_target = document.getElementById('spinnerContainer');
