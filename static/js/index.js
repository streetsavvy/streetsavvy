
var map = new ol.Map({
  target: 'map',
  layers: [
    new ol.layer.Tile({
      source: new ol.source.OSM()
      })
    ],
  view: new ol.View({
    center: ol.proj.fromLonLat([-122.4543, 37.7726]),
    zoom: 12,
    maxZoom: 17
  })
});
