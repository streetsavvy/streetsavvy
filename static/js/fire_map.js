var api_home = 'request/fire?' 

function getURL(){
    var end = document.getElementById("end").value;
    var start = document.getElementById("start").value;
    var re = new RegExp("^([0-9]{2}:(0[1-9]|11|12):[0-9]{4})$");
    console.log( end );
    console.log( start );
    if (!validateField(start, 'Start Date', re) || !validateField(end, 'End Date', re)){
        return null;
    }
    var api_url = api_home + 'start=' + start + '&stop=' + end;
    console.log(api_url);
    return api_url;
}

function addPoints(vectorSource, data){
    var new_points = [];
    var len = data.length;
    for (var i = 0; i < len; i++) {
        console.log( data[i].location.coordinates );
        console.log( data[i].call_type);
        var point = new ol.geom.Point( data[i].location.coordinates );
        point.transform('EPSG:4326', 'EPSG:900913');
        var new_point = new ol.Feature({
            geometry: point,
            name: data[i].call_type
        });
        new_points.push(new_point);
    }
    vectorSource.addFeatures( new_points );
}

function updateMap(vectorSource, url){
    vectorSource.clear()
    var spinner = new Spinner(opts).spin(spin_target);
    $.getJSON( url, function( data ) {
        console.log(data);
        if (typeof data === 'string' || data instanceof String){
            generate_alert(data);
        } else{
            addPoints(vectorSource, data);
        }
        spinner.stop();
    });
}

$(document).ready(function() {
    var fire_points = []
    var vectorSource = new ol.source.Vector({
        features: fire_points
    });
    var vectorLayer = new ol.layer.Vector({
        source: vectorSource
    });
    var map = new ol.Map({
         target: 'map',
         layers: [
             new ol.layer.Tile({
                 source: new ol.source.OSM()
             }),
             vectorLayer
         ],
         view: new ol.View({
             center: ol.proj.fromLonLat([-122.4543, 37.7726]),
             zoom: 12,
             maxZoom: 17
         })
    });
    
    $( '#submit' ).click(function() {
        var u = getURL();
        if (!u){
            vectorSource.clear();
        } else {
            updateMap(vectorSource, u);
        }
    } );
} );



