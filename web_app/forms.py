"""
   :platform: Unix
   :synopsis: Holdings the forms for submitting data using WTF Forms

.. moduleauthor:: Lauren Slason <lauren.slason@gmail.com>


"""
from datetime import (
    date,
    timedelta)

from flask_wtf import FlaskForm
from wtforms import DateField

def get_yesterday():
    """Return yesterday's date"""
    return date.today() - timedelta(1)

class DateQueryForm(FlaskForm):
    start = DateField('Start Date', format='%d:%m:%Y', default=get_yesterday)
    end = DateField('End Date', format='%d:%m:%Y', default=date.today)

