# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## [0.0.0] - 2016-10-22
### Added
 - Added a home bar with basic color scheme
