"""
   :platform: Unix
   :synopsis: Entry point for Street Savvy web app

.. moduleauthor:: Lauren Slason <lauren.slason@gmail.com>


"""

import json
import datetime
import argparse
import configparser

import requests
from flask import (
    Flask, 
    render_template, 
    redirect,
    request,
    flash,
    jsonify,
    url_for)

from web_app.forms import DateQueryForm

# Set configuration
PARSER = argparse.ArgumentParser(
    description='Provide configuration details for the API')
PARSER.add_argument('-c', type=str, dest='config', default='run.cfg')
ARGS = PARSER.parse_args()
CFG_PARSER = configparser.ConfigParser()
CFG_PARSER.read(ARGS.config)

APP = Flask(__name__)
APP.config['CSRF_ENABLED'] = CFG_PARSER['app']['csrf_enabled']
APP.config['SECRET_KEY'] = CFG_PARSER['app']['secret_key']


@APP.route("/")
def index():
    return render_template("index.html")

@APP.route("/request/<path:path>")
def send_to_api(path):
    query = str(request.query_string,'utf-8')
    url = '%s/api/%s?%s' % (CFG_PARSER['api']['url'], path, query)
    try:
        api_response = requests.get(url)
    except requests.ConnectionError:
        return json.dumps("API Connection Error")
    if api_response.status_code != 200:
        return json.dumps('API Error: %d' % api_response.status_code)
    if api_response.json() == []:
        return json.dumps("No Data Found")
    return json.dumps(api_response.json()).replace('null', '""')

@APP.route("/fire_table")
def fire_table():
    form = DateQueryForm()
    return render_template("fire_table.html", form=form)

@APP.route("/fire_map")
def fire_map():
    form = DateQueryForm()
    return render_template("fire_map.html", form=form)

if __name__ == "__main__":
    APP.run(host='0.0.0.0', debug=True)
